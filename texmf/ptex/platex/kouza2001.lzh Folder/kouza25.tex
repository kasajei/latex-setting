\documentclass[fleqn,a4j]{jarticle}
\usepackage{color}
\usepackage{emathPp}
\usepackage{emathAe}
\usepackage{emathR}
\usepackage{emathOld}
\usepackage{showexample}

\setcounter{section}{24}
\setcounter{page}{84}

\begin{document}
\section{\cmd{YGurafu}}
第\thesection 回は\textsf{perl}と連携して$y=f(x)$のグラフを描画する
\cmd{YGurafu}（96回）です。

\subsection{なぜ\cmd{YGurafu}}
\LaTeX の \textsf{picture}環境では，線分と円を描くことができますが，
それとても制約が多いものです。
\textsf{emathPh}では，\cmd{yGurafu}を用意しました。

\begin{showEx}{\cmd{yGurafu}}
\begin{zahyou}[ul=6mm]
(-1,3)(-3,2)
\def\Fx#1#2{\Sub{#1}{2}\y
  \Mul{#1}\y\y
  \Sub\y{1}#2}
\yGurafu\Fx\xmin\xmax
\end{zahyou}
\end{showEx}

上の例における関数\cmd{Fx}の定義がどんな式を定義しているのか，
すぐには分かりませんね。
そこで関数式の記述を簡潔にすべく\textsf{perl}にお出ましを願ったのが
\cmd{YGurafu}です。

\subsection{\cmd{YGurafu}}
先の例を\cmd{YGurafu}を用いて記述してみます。

\begin{showEx}{\cmd{YGurafu}}
\begin{zahyou}[ul=6mm]
(-1,3)(-3,2)
\def\Fx{X*(X-2)-1}
\YGurafu\Fx\xmin\xmax
\end{zahyou}
\end{showEx}

\subsection{式の記述法}
関数式の記述法について述べます。

\subsubsection{式の記述}
関数定義
\verb+\def\Fx{.....}+
の引数はそのまま\textsf{perl}に引き渡されますから，
\textsf{perl}の文法にしたがって記述することになります。
ただし，独立変数はxではなく，Xを用います。

\begin{jquote}
分数$\bunsuu23$は\verb+2/3+\\
平方根$\sqrt{5}$は\verb+sqrt(5)+\\
累乗根$\sqrt[3]{7}$は\verb+7**(1/3)+
\end{jquote}
と記述することもできます。

次の例は分数係数の二次関数
$y=\bunsuu{1}{2}x^2-\bunsuu{1}{3}x-\bunsuu{1}{4}$
のグラフ描画です。

\begin{showEx}{分数係数}
\begin{zahyou}[ul=6mm]
(-2,3)(-1,3)
\def\Fx{1/2*X*X-1/3*X-1/4}
\YGurafu\Fx\xmin\xmax
\end{zahyou}
\end{showEx}

累乗根の使用例として$y=(1-x^{\frac23})^{\frac32}$ ($0\leqq x\leqq 1$)
のグラフ描画を取り上げてみます。

\begin{showEx}{累乗根}
\begin{zahyou}[ul=12mm]
(-.2,1.5)(-.2,1.5)
\def\Fx{(1-X**(2/3))**(3/2)}
\YGurafu\Fx{0}{1}
\end{zahyou}
\end{showEx}

これはアステロイドの一部で，通常は媒介変数表示で描画しますが，
ここでは累乗根を使用してみました。

\subsubsection{円周率}
円周率$\pi$は\textsf{eclarith.sty}で
\begin{jquote}
\begin{verbatim}
\def\Pie{3.14159265}
\end{verbatim}
\end{jquote}
として定義されています。それを利用して
$y=\sin\pi x$のグラフを描画します。
\begin{showEx}{\cmd{Pie}}
\begin{zahyou}[ul=10mm]
(-.5,2.5)(-1.5,1.5)
\zahyouMemori[z]
\def\Fx{sin(\Pie*X)}
\YGurafu\Fx\xmin\xmax
\end{zahyou}
\end{showEx}

さきほど，\verb+\def\Fx{.....}+
の引数はそのまま\textsf{perl}に引き渡されると
書きましたが，\TeX の制御綴りは展開されて引き渡されますから，
上の例では，引数\verb+\Pie*X+は\verb+3.14159265*X+と展開されて
\textsf{perl}に引き渡されます。

また，\textsf{perl}では円周率は\verb+$pi+で定義されています。
次の例は$y=\sin2\pi x$のグラフ描画です。

\begin{showEx}{\texttt{\$pi}}
\begin{zahyou}[ul=10mm]
(-.5,2.5)(-1.5,1.5)
\zahyouMemori[z]
\def\Fx{sin(2*$pi*X)}
\YGurafu\Fx\xmin\xmax
\end{zahyou}
\end{showEx}

すなわち，\cmd{YGurafu}の関数定義において，円周率$\pi$は
\begin{jquote}
\begin{verbatim}
\Pie, $pi, 3.14159
\end{verbatim}
\end{jquote}
いずれでも同じ効果を持ちます。

なお，蛇足ながら，\textsf{perl}の三角関数における角の単位はラジアンです。

\subsubsection{指数}
べき乗は\textsf{perl}のべき乗演算子\verb+**+を用いるのが簡単です。

\begin{showEx}{$y=2^x$}
\begin{zahyou}[ul=4mm]
(-3,3)(-1,8)
\zahyouMemori[g]
\def\Fx{2**X}
\YGurafu\Fx\xmin\xmax
\end{zahyou}
\end{showEx}

自然対数の底$e$を底とする指数関数は\verb+exp(x)+です。

\begin{showEx}{$y=e^x$}
\begin{zahyou}[ul=4mm]
(-3,3)(-1,8)
\zahyouMemori[g]
\def\Fx{exp(X)}
\YGurafu\Fx\xmin\xmax
\end{zahyou}
\end{showEx}

自然対数の底$e$の値は\textsf{emathC}で
\begin{jquote}
\begin{verbatim}
\def\Napier{2.718281828}% 自然対数の底 e
\end{verbatim}
\end{jquote}
と定義されています。また，\textsf{perl}の変数として
\textsf{emath.pl}では
\begin{jquote}
\begin{verbatim}
# 自然対数の底
    $Napier=exp(1);
\end{verbatim}
\end{jquote}
と定義してあります。

\subsubsection{対数}
\textsf{perl}の対数関数\verb+log(x)+は自然対数です。

\begin{showEx}{$y=\log x$}
\begin{zahyou}[ul=4mm]
(-1,8)(-3,3)
\zahyouMemori[g]
\def\Fx{log(X)}
\YGurafu\Fx{.1}\xmax
\end{zahyou}
\end{showEx}

他の底に対する対数は\textsf{emath.pl}で
\begin{jquote}
\begin{verbatim}
log2(底,真数)
\end{verbatim}
\end{jquote}
を定義してあります。常用対数のグラフです。

\begin{showEx}{$y=\log_{10}x$}
\begin{zahyou}[ul=4mm]
(-1,11)(-2,2)
\zahyouMemori[g]
\def\Fx{log2(10,X)}
\YGurafu\Fx{.01}\xmax
\end{zahyou}
\end{showEx}

\subsubsection{\cmd{XGurafu}}
接線が$x$軸に垂直になる場合は，\cmd{YGurafu}より\cmd{XGurafu}を
用いる方が適切です。

$y=\sqrt x$のグラフを\cmd{YGurafu}を用いて描画すると

\begin{showEx}{$y=\sqrt x$}
\begin{zahyou}[ul=16mm]
(-.5,1.5)(-.5,1.5)
\def\Fx{sqrt(X)}
\YGurafu\Fx{0}\xmax
\end{zahyou}
\end{showEx}

原点の近くが折れ線になっています。
これを\cmd{XGurafu}を用いてみましょう。

\begin{showEx}{$x=y^2$}
\begin{zahyou}[ul=16mm]
(-.5,1.5)(-.5,1.5)
\def\Fy{Y*Y}
\XGurafu\Fy{0}\xmax
\end{zahyou}
\end{showEx}

原点の近傍が滑らかになりました。
このように，微分係数が無限大になるところでは，
\cmd{YGurafu}ではなく，\cmd{XGurafu}, \cmd{BGurafu} などを用いると
よいでしょう。

\subsection{実例}
大学入試問題から拾ってみます。

\openKaiFile
\begin{enumerate}[\Large 1.~]
  \item \ReadTeXFile{0072200108.tex}
  \item \ReadTeXFile{0009200104.tex}
  \item \ReadTeXFile{2137200118.tex}
\end{enumerate}
\closeKaiFile
\end{document}
