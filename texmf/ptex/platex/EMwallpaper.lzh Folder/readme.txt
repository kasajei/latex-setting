wallpaper.sty と emathPh.sty とは相容れない部分があります。
emath パッケージと併用を可能とした修正ファイルが
    EMwallpaper.sty
    EMeso-pic.sty
です。
    example.tex
は，その EMwallpaper.sty を呼び出すように修正したものです。

なお，原配布の画像形式は png ですが，
ここには eps形式に変換したものを同梱しています。
