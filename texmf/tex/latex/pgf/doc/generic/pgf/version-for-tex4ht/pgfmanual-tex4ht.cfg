% Copyright 2005 by Till Tantau <tantau@cs.tu-berlin.de>.
%
% This program can be redistributed and/or modified under the terms
% of the LaTeX Project Public License Distributed from CTAN
% archives in directory macros/latex/base/lppl.txt.

% Make sure to use tex4ht (or htlatex, to be precise) to compile this.

\usepackage{tex4ht}
\def\pgfsysdriver{pgfsys-tex4ht.def}

\usepackage{xkeyval}
\usepackage{colortbl}

\let\originalsetkeys=\setkeys

\makeatletter

\AtBeginDocument{
\let\setkeys=\originalsetkeys
\CutAt{section,part}
\definecolor{graphicbackground}{rgb}{0.96,0.96,0.8}
\definecolor{codebackground}{rgb}{0.8,0.8,1}
\definecolor{declarebg}{rgb}{1,0.8,0.8}
\definecolor{gray}{gray}{.5}

% Patches of macros...
\renewenvironment{pgfmanualentry}{\begin{description}}{\end{description}}
\renewcommand\pgfmanualentryheadline[1]{\item[\colorbox{declarebg}{\normalfont{#1}}]\par}
\renewcommand\pgfmanualbody{}

\def\endofcodeexample#1{%
  \endgroup%
  \ifcode@execute%
    \setbox\codeexamplebox=\hbox{%
      {%
        {%
          \returntospace%
          \commenthandler%
          \xdef\code@temp{#1}% removes returns and comments
        }%
        \color{black}\ignorespaces%
          \expandafter\scantokens\expandafter{\code@temp\ignorespaces}\ignorespaces%
      }%
    }%
    \ifdim\wd\codeexamplebox>\codeexamplewidth%
      \box\codeexamplebox
      \begin{tabular}{>{\columncolor{codebackground}}l}
        \begin{minipage}{\textwidth}
      {%
        \let\do\@makeother
        \dospecials
        \frenchspacing\@vobeyspaces
        \normalfont\ttfamily\footnotesize
        \typesetcomment%
        \@tempswafalse
        \def\par{%
          \if@tempswa
          \leavevmode \null \@@par\penalty\interlinepenalty
          \else
          \@tempswatrue
          \ifhmode\@@par\penalty\interlinepenalty\fi
          \fi}%
        \obeylines
        \everypar \expandafter{\the\everypar \unpenalty}%
        #1}
        \end{minipage}
      \end{tabular}%
    \else
      \begin{tabular}{>{\columncolor{graphicbackground}}l>{\columncolor{codebackground}}l}
        \ \box\codeexamplebox\ \par &
        \begin{minipage}[t]{\textwidth}
        {\let\do\@makeother
        \dospecials
        \frenchspacing\@vobeyspaces
        \normalfont\ttfamily\footnotesize
        \typesetcomment%
        \@tempswafalse
        \def\par{%
          \if@tempswa
          \leavevmode \null \@@par\penalty\interlinepenalty
          \else
          \@tempswatrue
          \ifhmode\@@par\penalty\interlinepenalty\fi
          \fi}%
        \obeylines
        \everypar \expandafter{\the\everypar \unpenalty}%
        #1}
        \end{minipage}
      \end{tabular}
    \fi
  \else%
    \begin{tabular}{>{\columncolor{codebackground}}l}
      \begin{minipage}[t]{\textwidth}
      {%
        \let\do\@makeother
        \dospecials
        \frenchspacing\@vobeyspaces
        \normalfont\ttfamily\footnotesize
        \typesetcomment%
        \@tempswafalse
        \def\par{%
          \if@tempswa
          \leavevmode \null \@@par\penalty\interlinepenalty
          \else
          \@tempswatrue
          \ifhmode\@@par\penalty\interlinepenalty\fi
          \fi}%
        \obeylines
        \everypar \expandafter{\the\everypar \unpenalty}%
        #1}
       \end{minipage}
     \end{tabular}%
  \fi%
  \par%
  \end{codeexample}
}
}



\makeatother
