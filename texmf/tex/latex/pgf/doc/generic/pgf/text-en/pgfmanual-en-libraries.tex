% Copyright 2003 by Till Tantau <tantau@cs.tu-berlin.de>.
%
% This program can be redistributed and/or modified under the terms
% of the LaTeX Project Public License Distributed from CTAN
% archives in directory macros/latex/base/lppl.txt.


\section{Libraries}

\subsection{Arrow Tip Library}
\label{section-library-arrows}

\begin{package}{pgflibraryarrows}
  The package defines additional arrow tips, which are described
  below. See page~\pageref{standard-arrows} for the arrows tips that
  are defined by default. Note that neither the standard packages nor
  this package defines an arrow name containing |>| or |<|. These are
  left for the user to defined as he or she sees fit.
\end{package}

\subsubsection{Triangular Arrow Tips}

\begin{tabular}{ll}
  \symarrow{latex'} \\
  \symarrow{latex' reversed}  \\
  \symarrow{stealth'} \\
  \symarrow{stealth' reversed}\\
  \symarrow{triangle 90} \\
  \symarrow{triangle 90 reversed}   \\
  \symarrow{triangle 60} \\
  \symarrow{triangle 60 reversed}   \\
  \symarrow{triangle 45} \\
  \symarrow{triangle 45 reversed}   \\
  \symarrow{open triangle 90} \\
  \symarrow{open triangle 90 reversed}   \\
  \symarrow{open triangle 60} \\
  \symarrow{open triangle 60 reversed}   \\
  \symarrow{open triangle 45} \\
  \symarrow{open triangle 45 reversed}   \\
\end{tabular}

\subsubsection{Barbed Arrow Tips}

\begin{tabular}{ll}
  \symarrow{angle 90} \\
  \symarrow{angle 90 reversed}   \\
  \symarrow{angle 60} \\
  \symarrow{angle 60 reversed}   \\
  \symarrow{angle 45} \\
  \symarrow{angle 45 reversed}   \\
  \symarrow{hooks} \\
  \symarrow{hooks reversed} \\
\end{tabular}


\subsubsection{Bracket-Like Arrow Tips}

\begin{tabular}{ll}
  \sarrow{[}{]} \\
  \sarrow{]}{[} \\
  \sarrow{(}{)} \\
  \sarrow{)}{(}
\end{tabular}

\subsubsection{Circle and Diamond Arrow Tips}


\begin{tabular}{ll}
  \symarrow{o} \\
  \symarrow{*} \\
  \symarrow{diamond} \\
  \symarrow{open diamond}   \\
\end{tabular}


\subsubsection{Partial Arrow Tips}

\begin{tabular}{ll}
  \symarrow{left to} \\
  \symarrow{left to reversed} \\
  \symarrow{right to} \\
  \symarrow{right to reversed} \\
  \symarrow{left hook} \\
  \symarrow{left hook reversed} \\
  \symarrow{right hook} \\
  \symarrow{right hook reversed}
\end{tabular}


\subsubsection{Line Caps}

\begin{tabular}{ll}
  \carrow{round cap} \\
  \carrow{butt cap} \\
  \carrow{triangle 90 cap} \\
  \carrow{triangle 90 cap reversed} \\
  \carrow{fast cap} \\
  \carrow{fast cap reversed} \\
\end{tabular}


\subsection{Snake Library}

\label{section-library-snakes}

\begin{package}{pgflibrarysnakes}
  This library package defines basic
  snakes. Section~\ref{section-tikz-snakes} explains how snakes are
  used in \tikzname, Section~\ref{section-base-snakes} explains how
  new snakes can be defined.

  The snakes are influenced by the current values of parameters like
  |\pgfsnakesegmentamplitude|. Only this parameter and
  |\pgfsnakesegmentlength| are proper \TeX\ dimensions, all other
  parameters are \TeX\ macros.

  In \tikzname, each parameter can be set using an option having the
  parameters name minus the |\pgfsnake| part.
\end{package}


\begin{snake}{border}
  This snake adds straight lines the path that are at a specific angle
  to the line toward the target. The idea is to add these little lines
  to indicate the ``border'' or an area. The following parameters
  influence the snake:  
  \begin{itemize}
  \item |\pgfsnakesegmentlength|
    determines the distance between consecutive ticks.
  \item |\pgfsnakesegmentamplitude|
    determines the length of the ticks.
  \item |\pgfsnakesegmentangle|
    determines the angle between the ticks and the line toward the
    target. 
  \end{itemize}
\begin{codeexample}[]
\tikz{\draw (0,0) rectangle (3,1)
            [snake=border,segment angle=-45] (0,0) rectangle (3,1);}
\end{codeexample}
\end{snake}


\begin{snake}{brace}
  This snake adds a long brace to the path. The left and right end of
  the brace will be exactly on the start and endpoint of the
  snake. The following parameters influence the snake:  
  \begin{itemize}
  \item |\pgfsnakesegmentamplitude|
    determines how much the brace rises above the path.
  \item |\pgfsnakesegmentaspect|
    determines the fraction of the total length where the ``middle
    part'' of the brace will be.  
  \end{itemize}
\begin{codeexample}[]
\tikz{\draw[snake=brace,segment aspect=0.25] (0,0) -- (3,0);}
\end{codeexample}
\end{snake}

\begin{snake}{bumps}
  This snake consists of little half ellipses. The following parameters
  influence the snake:
  \begin{itemize}
  \item |\pgfsnakesegmentamplitude|
    determines the height of the half ellipse.
  \item |\pgfsnakesegmentlength|
    determines the width of the half ellipse.
  \end{itemize}
\begin{codeexample}[]
\tikz{\draw[snake=bumps] (0,0) -- (3,0);}
\end{codeexample}
\end{snake}


\begin{snake}{coil}
  This snake adds a coil to the path. To understand how this works,
  imagine a three-dimensional spring. The spring's axis points along
  the line toward the target. Then, we ``view'' the spring from a
  certain angle. If we look ``straight from the side'' we will see a
  perfect sine curve, if we look ``more from the front'' we will see a
  coil. The following parameters influence the snake:  
  \begin{itemize}
  \item |\pgfsnakesegmentamplitude|
    determines how much the coil rises above the path and falls below
    it. Thus, this is the radius of the coil.
  \item |\pgfsnakesegmentlength|
    determines the distance between two consecutive ``curls.'' Thus,
    when the spring is see ``from the side'' this will be the wave
    length of the sine curve. 
  \item |\pgfsnakesegmentaspect|
    determines the ``viewing direction.'' A value of |0| means
    ``looking from the side'' and a value of |0.5|, which is the
    default, means ``look more from the front.'' 
  \end{itemize}
\begin{codeexample}[]
\begin{tikzpicture}[segment amplitude=10pt]
  \draw[snake=coil]                  (0,1) -- (3,1);
  \draw[snake=coil,segment aspect=0] (0,0) -- (3,0);
\end{tikzpicture}
\end{codeexample}
\end{snake}


\begin{snake}{expanding waves}
  This snake adds arcs to the path that get bigger along the line
  towards the target. The following parameters influence the snake:
  \begin{itemize}
  \item |\pgfsnakesegmentlength|
    determines the distance between consecutive arcs.
  \item |\pgfsnakesegmentangle|
    determines the opening angle below and above the path. Thus, the
    total opening angle is twice this angle.
  \end{itemize}
\begin{codeexample}[]
\tikz{\draw[snake=expanding waves] (0,0) -- (3,0);}
\end{codeexample}
\end{snake}


\begin{snake}{saw}
  This snake looks like the blade of a saw. The following parameters
  influence the snake:
  \begin{itemize}
  \item |\pgfsnakesegmentamplitude|
    determines how much each spike raises above the straight line.
  \item |\pgfsnakesegmentlength|
    determines the length each spike.
  \end{itemize}
\begin{codeexample}[]
\tikz{\draw[snake=saw] (0,0) -- (3,0);}
\end{codeexample}
\end{snake}


\begin{snake}{snake}
  This snake is the ``architypical'' snake: It looks like a snake seen
  from above. More precisely, the snake is a sine wave with a
  ``softened'' start and ending. The following parameters influence
  the snake: 
  \begin{itemize}
  \item |\pgfsnakesegmentamplitude|
    determines the sine wave's amplitude.
  \item |\pgfsnakesegmentlength|
    determines the sine wave's wave length.
  \end{itemize}
\begin{codeexample}[]
\tikz{\draw[snake=snake] (0,0) -- (3,0);}
\end{codeexample}
\end{snake}


\begin{snake}{ticks}
  This snake adds straight lines  the path that are orthogonal to the
  line toward the target. The following parameters influence the snake: 
  \begin{itemize}
  \item |\pgfsnakesegmentlength|
    determines the distance between consecutive ticks.
  \item |\pgfsnakesegmentamplitude|
    determines half the length of the ticks.
  \end{itemize}
\begin{codeexample}[]
\tikz{\draw[snake=ticks] (0,0) -- (3,0);}
\end{codeexample}
\end{snake}
\begin{snake}{triangles}
  This snake adds triangles to the path that point toward the
  target. The following parameters influence the snake: 
  \begin{itemize}
  \item |\pgfsnakesegmentlength|
    determines the distance between consecutive triangles.
  \item |\pgfsnakesegmentamplitude|
    determines half the length of the triangle side that is orthogonal
    to the path.
  \item |\pgfsnakesegmentobjectlength|
    determines the height of the triangle.
  \end{itemize}
\begin{codeexample}[]
\tikz{\draw[snake=triangles] (0,0) -- (3,0);}
\end{codeexample}
\end{snake}


\begin{snake}{waves}
  This snake adds arcs to the path that have a constant size. The
  following parameters influence the snake: 
  \begin{itemize}
  \item |\pgfsnakesegmentlength|
    determines the distance between consecutive arcs.
  \item |\pgfsnakesegmentangle|
    determines the opening angle below and above the path. Thus, the
    total opening angle is twice this angle.
  \item |\pgfsnakesegmentamplitude|
    determines the radius of each arc.
  \end{itemize}
\begin{codeexample}[]
\tikz{\draw[snake=waves] (0,0) -- (3,0);}
\end{codeexample}
\end{snake}


\begin{snake}{zigzag}
  This snake looks like a zig-zag line. The following parameters
  influence the snake:
  \begin{itemize}
  \item |\pgfsnakesegmentamplitude|
    determines how much the zig-zag lines raises above and falls below
    a straight line to the target point.
  \item |\pgfsnakesegmentlength|
    determines the length of a complete ``up-down'' cycle.
  \end{itemize}
\begin{codeexample}[]
\tikz{\draw[snake=zigzag] (0,0) -- (3,0);}
\end{codeexample}
\end{snake}



\subsection{Plot Handler Library}
\label{section-library-plothandlers}

\begin{package}{pgflibraryplothandlers}
  This library packages defines additional plot handlers, see
  Section~\ref{section-plot-handlers} for an introduction to plot
  handlers. The additional handlers are described in the following. 
\end{package}


\subsubsection{Curve Plot Handlers}
  
\begin{command}{\pgfplothandlercurveto}
  This handler will issue a |\pgfpathcurveto| command for each point of
  the plot, \emph{except} possibly for the first. As for the line-to
  handler, what happens with the first point can be specified using
  |\pgfsetmovetofirstplotpoint| or |\pgfsetlinetofirstplotpoint|.

  Obviously, the |\pgfpathcurveto| command needs, in addition to the
  points on the path, some control points. These are generated
  automatically using a somewhat ``dumb'' algorithm: Suppose you have
  three points $x$, $y$, and $z$ on the curve such that $y$ is between
  $x$ and $z$:
\begin{codeexample}[]
\begin{tikzpicture}    
  \draw[gray] (0,0) node {x} (1,1) node {y} (2,.5) node {z};
  \pgfplothandlercurveto
  \pgfplotstreamstart
  \pgfplotstreampoint{\pgfpoint{0cm}{0cm}}
  \pgfplotstreampoint{\pgfpoint{1cm}{1cm}}
  \pgfplotstreampoint{\pgfpoint{2cm}{.5cm}}
  \pgfplotstreamend
  \pgfusepath{stroke}
\end{tikzpicture}
\end{codeexample}

  In order to determine the control points of the curve at the point
  $y$, the handler computes the vector $z-x$ and scales it by the
  tension factor (see below). Let us call the resulting vector
  $s$. Then $y+s$ and $y-s$ will be the control points around $y$. The
  first control point at the beginning of the curve will be the
  beginning itself, once more; likewise the last control point is the
  end itself.
\end{command}

\begin{command}{\pgfsetplottension\marg{value}}
  Sets the factor used by the curve plot handlers to determine the
  distance of the control points from the points they control. The
  higher the curvature of the curve points, the higher this value
  should be. A value of $1$ will cause four points at quarter
  positions of a circle to be connected using a circle. The default is
  $0.5$. 

\begin{codeexample}[]
\begin{tikzpicture}    
  \draw[gray] (0,0) node {x} (1,1) node {y} (2,.5) node {z};
  \pgfsetplottension{0.75}
  \pgfplothandlercurveto
  \pgfplotstreamstart
  \pgfplotstreampoint{\pgfpoint{0cm}{0cm}}
  \pgfplotstreampoint{\pgfpoint{1cm}{1cm}}
  \pgfplotstreampoint{\pgfpoint{2cm}{0.5cm}}
  \pgfplotstreamend
  \pgfusepath{stroke}
\end{tikzpicture}
\end{codeexample}
\end{command}


\begin{command}{\pgfplothandlerclosedcurve}
  This handler works like the curve-to plot handler, only it will
  add a new part to the current path that is a closed curve through
  the plot points.
\begin{codeexample}[]
\begin{tikzpicture}    
  \draw[gray] (0,0) node {x} (1,1) node {y} (2,.5) node {z};
  \pgfplothandlerclosedcurve
  \pgfplotstreamstart
  \pgfplotstreampoint{\pgfpoint{0cm}{0cm}}
  \pgfplotstreampoint{\pgfpoint{1cm}{1cm}}
  \pgfplotstreampoint{\pgfpoint{2cm}{0.5cm}}
  \pgfplotstreamend
  \pgfusepath{stroke}
\end{tikzpicture}
\end{codeexample}
\end{command}


\subsubsection{Comb Plot Handlers}

There are three ``comb'' plot handlers. There name stems from the fact
that the plots they produce look like ``combs'' (more or less).

\begin{command}{\pgfplothandlerxcomb}
  This handler converts each point in the plot stream into a line from
  the $y$-axis to the point's coordinate, resulting in a ``horizontal
  comb.''

  
\begin{codeexample}[]
\begin{tikzpicture}    
  \draw[gray] (0,0) node {x} (1,1) node {y} (2,.5) node {z};
  \pgfplothandlerxcomb
  \pgfplotstreamstart
  \pgfplotstreampoint{\pgfpoint{0cm}{0cm}}
  \pgfplotstreampoint{\pgfpoint{1cm}{1cm}}
  \pgfplotstreampoint{\pgfpoint{2cm}{0.5cm}}
  \pgfplotstreamend
  \pgfusepath{stroke}
\end{tikzpicture}
\end{codeexample}
\end{command}


\begin{command}{\pgfplothandlerycomb}
  This handler converts each point in the plot stream into a line from
  the $x$-axis to the point's coordinate, resulting in a ``vertical
  comb.''
  
  This handler is useful for creating ``bar diagrams.''
\begin{codeexample}[]
\begin{tikzpicture}    
  \draw[gray] (0,0) node {x} (1,1) node {y} (2,.5) node {z};
  \pgfplothandlerycomb
  \pgfplotstreamstart
  \pgfplotstreampoint{\pgfpoint{0cm}{0cm}}
  \pgfplotstreampoint{\pgfpoint{1cm}{1cm}}
  \pgfplotstreampoint{\pgfpoint{2cm}{0.5cm}}
  \pgfplotstreamend
  \pgfusepath{stroke}
\end{tikzpicture}
\end{codeexample}
\end{command}

\begin{command}{\pgfplothandlerpolarcomb}
  This handler converts each point in the plot stream into a line from
  the origin to the point's coordinate.
  
\begin{codeexample}[]
\begin{tikzpicture}    
  \draw[gray] (0,0) node {x} (1,1) node {y} (2,.5) node {z};
  \pgfplothandlerpolarcomb
  \pgfplotstreamstart
  \pgfplotstreampoint{\pgfpoint{0cm}{0cm}}
  \pgfplotstreampoint{\pgfpoint{1cm}{1cm}}
  \pgfplotstreampoint{\pgfpoint{2cm}{0.5cm}}
  \pgfplotstreamend
  \pgfusepath{stroke}
\end{tikzpicture}
\end{codeexample}
\end{command}

\subsubsection{Mark Plot Handler}

\label{section-plot-marks}

\begin{command}{\pgfplothandlermark\marg{mark code}}
  This command will execute the \meta{mark code} for each point of the
  plot, but each time the coordinate transformation matrix will be
  setup such that the origin is at the position of the point to be
  plotted. This way, if the \meta{mark code} draws a little circle
  around the origin, little circles will be drawn at each point of the
  plot.
  
\begin{codeexample}[]
\begin{tikzpicture}    
  \draw[gray] (0,0) node {x} (1,1) node {y} (2,.5) node {z};
  \pgfplothandlermark{\pgfpathcircle{\pgfpointorigin}{4pt}\pgfusepath{stroke}}
  \pgfplotstreamstart
  \pgfplotstreampoint{\pgfpoint{0cm}{0cm}}
  \pgfplotstreampoint{\pgfpoint{1cm}{1cm}}
  \pgfplotstreampoint{\pgfpoint{2cm}{0.5cm}}
  \pgfplotstreamend
  \pgfusepath{stroke}
\end{tikzpicture}
\end{codeexample}

  Typically, the \meta{code} will be |\pgfuseplotmark{|\meta{plot mark
      name}|}|, where \meta{plot mark name} is the name of a
  predefined plot mark.
\end{command}

\begin{command}{\pgfuseplotmark\marg{plot mark name}}
  Draws the given \meta{plot mark name} at the origin. The \meta{plot
    mark name} must previously have been declared using
  |\pgfdeclareplotmark|. 

\begin{codeexample}[]
\begin{tikzpicture}    
  \draw[gray] (0,0) node {x} (1,1) node {y} (2,.5) node {z};
  \pgfplothandlermark{\pgfuseplotmark{pentagon}}
  \pgfplotstreamstart
  \pgfplotstreampoint{\pgfpoint{0cm}{0cm}}
  \pgfplotstreampoint{\pgfpoint{1cm}{1cm}}
  \pgfplotstreampoint{\pgfpoint{2cm}{0.5cm}}
  \pgfplotstreamend
  \pgfusepath{stroke}
\end{tikzpicture}
\end{codeexample}
\end{command}

\begin{command}{\pgfdeclareplotmark\marg{plot mark name}\marg{code}}
  Declares a plot mark for later used with the |\pgfuseplotmark|
  command.

\begin{codeexample}[]
\pgfdeclareplotmark{my plot mark}
  {\pgfpathcircle{\pgfpoint{0cm}{1ex}}{1ex}\pgfusepathqstroke}  
\begin{tikzpicture}    
  \draw[gray] (0,0) node {x} (1,1) node {y} (2,.5) node {z};
  \pgfplothandlermark{\pgfuseplotmark{my plot mark}}
  \pgfplotstreamstart
  \pgfplotstreampoint{\pgfpoint{0cm}{0cm}}
  \pgfplotstreampoint{\pgfpoint{1cm}{1cm}}
  \pgfplotstreampoint{\pgfpoint{2cm}{0.5cm}}
  \pgfplotstreamend
  \pgfusepath{stroke}
\end{tikzpicture}
\end{codeexample}
\end{command}


\begin{command}{\pgfsetplotmarksize\marg{dimension}}
  This command sets the \TeX\ dimension |\pgfplotmarksize| to
  \meta{dimension}. This dimension is a ``recommendation'' for plot
  mark code at which size the plot mark should be drawn; plot mark
  code may choose to ignore this \meta{dimension} altogether. For
  circles, \meta{dimension} should  be the radius, for other shapes it
  should be about half the width/height.

  The predefined plot marks all take this dimension into account.

\begin{codeexample}[]
\begin{tikzpicture}    
  \draw[gray] (0,0) node {x} (1,1) node {y} (2,.5) node {z};
  \pgfsetplotmarksize{1ex}
  \pgfplothandlermark{\pgfuseplotmark{*}}
  \pgfplotstreamstart
  \pgfplotstreampoint{\pgfpoint{0cm}{0cm}}
  \pgfplotstreampoint{\pgfpoint{1cm}{1cm}}
  \pgfplotstreampoint{\pgfpoint{2cm}{0.5cm}}
  \pgfplotstreamend
  \pgfusepath{stroke}
\end{tikzpicture}
\end{codeexample}
\end{command}

\begin{textoken}{\pgfplotmarksize}
  A \TeX\ dimension that is a ``recommendation'' for the size of plot
  marks.
\end{textoken}

The following plot marks are predefined (the filling color has been
set to yellow):

\medskip
\begin{tabular}{lc}
  \plotmarkentry{*}
  \plotmarkentry{x}
  \plotmarkentry{+}
\end{tabular}


\subsection{Plot Mark Library}

\begin{package}{pgflibraryplotmarks}
  When this package is loaded, the following plot marks are defined in
  addition to |*|, |x|, and |+| (the filling color has been set to a
  dark yellow):

  \catcode`\|=12
  \medskip
  \begin{tabular}{lc}
    \plotmarkentry{-}
    \index{*vbar@\protect\texttt{\protect\myvbar} plot mark}%
    \index{Plot marks!*vbar@\protect\texttt{\protect\myvbar}}
    \texttt{\char`\\pgfuseplotmark\char`\{\declare{|}\char`\}} &
    \tikz\draw[color=black!25] plot[mark=|,mark options={fill=yellow,draw=black}]
    coordinates {(0,0) (.5,0.2) (1,0) (1.5,0.2)};\\
    \plotmarkentry{o}
    \plotmarkentry{asterisk}
    \plotmarkentry{star}
    \plotmarkentry{oplus}
    \plotmarkentry{oplus*}
    \plotmarkentry{otimes}
    \plotmarkentry{otimes*}
    \plotmarkentry{square}
    \plotmarkentry{square*}
    \plotmarkentry{triangle}
    \plotmarkentry{triangle*}
    \plotmarkentry{diamond}
    \plotmarkentry{diamond*}
    \plotmarkentry{pentagon}
    \plotmarkentry{pentagon*}
  \end{tabular}
\end{package}



\subsection{Shape Library}

\begin{package}{pgflibraryshapes}
  This library packages defines additional shapes, which  are
  described in the following.  
\end{package}

\begin{shape}{cross out}
  This shape ``crosses out'' the node. Its foreground path are simply
  two diagonal lines that between the corners of the node's bounding
  box. Here is an example:

\begin{codeexample}[]
\begin{tikzpicture}
  \draw[help lines] (0,0) grid (3,2);
  \node [cross out,draw=red] at (1.5,1) {cross out};
\end{tikzpicture}
\end{codeexample}

  A useful application is inside text as in the following example:
\begin{codeexample}[]
Cross \tikz[baseline] \node [cross out,draw,anchor=text] {me}; out!  
\end{codeexample}

  This shape inherits all anchors from the |rectangle| shape, see also
  the following figure:
\begin{codeexample}[]
\Huge
\begin{tikzpicture}
  \node[name=s,shape=cross out,style=shape example] {cross out\vrule width 1pt height 2cm};
  \foreach \anchor/\placement in
    {north west/above left, north/above, north east/above right, 
     west/left, center/above, east/right, 
     mid west/right, mid/above, mid east/left, 
     base west/left, base/below, base east/right, 
     south west/below left, south/below, south east/below right, 
     text/left, 10/right, 130/above}
     \draw[shift=(s.\anchor)] plot[mark=x] coordinates{(0,0)}
       node[\placement] {\scriptsize\texttt{(s.\anchor)}};
\end{tikzpicture}
\end{codeexample}
\end{shape}

\begin{shape}{ellipse}
  This shape is an ellipse tightly fitting the text box, if no inner
  separation is given. The following figure shows the anchors this
  shape defines; the anchors |10| and |130| are example of border anchors.
\begin{codeexample}[]
\Huge
\begin{tikzpicture}
  \node[name=s,shape=ellipse,style=shape example] {Ellipse\vrule width 1pt height 2cm};
  \foreach \anchor/\placement in
    {north west/above left, north/above, north east/above right, 
     west/left, center/above, east/right, 
     mid west/right, mid/above, mid east/left, 
     base west/left, base/below, base east/right, 
     south west/below left, south/below, south east/below right, 
     text/left, 10/right, 130/above}
     \draw[shift=(s.\anchor)] plot[mark=x] coordinates{(0,0)}
       node[\placement] {\scriptsize\texttt{(s.\anchor)}};
\end{tikzpicture}
\end{codeexample}
\end{shape}

\begin{shape}{forbidden sign}
  This shape places the node inside a circle with a diagonal from the
  lower left to the upper right added. The circle is part of the
  background, the diagonal line part of the foreground path; thus, the
  diagonal line is on top of the text.
  
\begin{codeexample}[]
\begin{tikzpicture}
  \node [forbidden sign,line width=1ex,draw=red,fill=white] {Smoking};
\end{tikzpicture}
\end{codeexample}

  The shape inherits all anchors from the |circle| shape, see also the
  following figure:
\begin{codeexample}[]
\Huge
\begin{tikzpicture}
  \node[name=s,shape=forbidden sign,style=shape example] {Forbidden\vrule width 1pt height 2cm};
  \foreach \anchor/\placement in
    {north west/above left, north/above, north east/above right, 
     west/left, center/above, east/right, 
     mid west/right, mid/above, mid east/left, 
     base west/left, base/below, base east/right, 
     south west/below left, south/below, south east/below right, 
     text/left, 10/right, 130/above}
     \draw[shift=(s.\anchor)] plot[mark=x] coordinates{(0,0)}
       node[\placement] {\scriptsize\texttt{(s.\anchor)}};
\end{tikzpicture}
\end{codeexample}
\end{shape}


\begin{shape}{strike out}
  This shape is idential to the |cross out| shape, only its foreground
  path consists of a single line from the lower left to the upper
  right.
  
\begin{codeexample}[]
Strike \tikz[baseline] \node [strike out,draw,anchor=text] {me}; out!  
\end{codeexample}

  See the |cross out| shape for the anchors.
\end{shape}



\subsection{Tree Library}

\label{section-tree-library}


\begin{package}{pgflibrarytikztrees}
  This packages defines styles to be used when drawing trees. 
\end{package}

\subsubsection{Growth Functions}

The package |pgflibrarytikztrees| defines two new growth
functions. They are installed using the following options:

\begin{itemize}
  \itemoption{grow via three points}|=one child at (|\meta{x}%
  |) and two children at (|\meta{y}|) and (|\meta{z}|)|
  This option installs a growth function that works as follows: If a
  parent node has just one child, this child is placed at \meta{x}. If
  the parent node has two children, these are placed at \meta{y} and
  \meta{z}. If the parent node has more than two children, the
  children are placed at points that are linearly extrapolated from
  the three points \meta{x}, \meta{y}, and \meta{z}. In detail, the
  position is $x + \frac{n-1}{2}(y-x) + (c-1)(z-y)$, where $n$ is the
  number of children and $c$ is the number of the current child
  (starting with~$1$).

  The net effect of all this is that if you have a certain ``linear
  arrangement'' in mind and use this option to specify the placement
  of a single child and of two children, then any number of children
  will be placed correctly.

  Here are some arrangements based on this growth function. We start
  with a simple ``above'' arrangement:
\begin{codeexample}[]
\begin{tikzpicture}[grow via three points={%
    one child at (0,1) and two children at (-.5,1) and (.5,1)}]
  \node at (0,0) {one} child;
  \node at (0,-1.5) {two} child child;
  \node at (0,-3) {three} child child child;
  \node at (0,-4.5) {four} child child child child;
\end{tikzpicture}
\end{codeexample}    

  The next arrangement places children above, but ``grows only to the
  right.'' 
\begin{codeexample}[]
\begin{tikzpicture}[grow via three points={%
    one child at (0,1) and two children at (0,1) and (1,1)}]
  \node at (0,0) {one} child;
  \node at (0,-1.5) {two} child child;
  \node at (0,-3) {three} child child child;
  \node at (0,-4.5) {four} child child child child;
\end{tikzpicture}
\end{codeexample}    

  In the final arrangement, the children are placed along a line going
  down and right.
\begin{codeexample}[]
\begin{tikzpicture}[grow via three points={%
    one child at (-1,-.5) and two children at (-1,-.5) and (0,-.75)}]
  \node at (0,0) {one} child;
  \node at (0,-1.5) {two} child child;
  \node at (0,-3) {three} child child child;
  \node at (0,-4.5) {four} child child child child;
\end{tikzpicture}
\end{codeexample}

  These examples should make it clear how you can create new styles to
  arrange your children along a line.

  \itemstyle{grow cyclic}
  This style causes the children to be arranged ``on a circle.'' For
  this, the children are placed at distance |\tikzleveldistance| from
  the parent node, but not on a straight line, but points on a
  circle. Instead of a sibling distance, there is a |sibling angle|
  that denotes the angle between two given children.
  \begin{itemize}
    \itemoption{sibling angle}|=|\meta{angle}
    Sets the angle between siblings in the |grow cyclic| style.
  \end{itemize}
  Note that this function will rotate the coordinate system of the
  children to ensure that the grandchildren will grow in the right
  direction.
\begin{codeexample}[]
\begin{tikzpicture}[grow cyclic]
  \tikzstyle{level 1}=[level distance=8mm,sibling angle=60]
  \tikzstyle{level 2}=[level distance=4mm,sibling angle=45]
  \tikzstyle{level 3}=[level distance=2mm,sibling angle=30]
  \coordinate [rotate=-90] % going down
    child foreach \x in {1,2,3}
      {child foreach \x in {1,2,3}
        {child foreach \x in {1,2,3}}};
\end{tikzpicture}
\end{codeexample}
\end{itemize}

\subsubsection{Edges From Parent}

The following styles can be used to modify how the edges from parents
are drawn:

\begin{itemize}
  \itemstyle{edge from parent fork down}
  This style will draw a line from the parent downwards (for half the
  level distance) and then on to the child using only horizontal and
  vertical lines. 
\begin{codeexample}[]
\begin{tikzpicture}
  \node {root}
    [edge from parent fork down]
    child {node {left}}
    child {node {right}
      child[child anchor=north east] {node {child}}
      child {node {child}}
    };
\end{tikzpicture}
\end{codeexample}
  \itemstyle{edge from parent fork right}
  This style behaves similarly, only it will first draw its edge to
  the right.
\begin{codeexample}[]
\begin{tikzpicture}
  \node {root}
    [edge from parent fork right,grow=right]
    child {node {left}}
    child {node {right}
      child {node {child}}
      child {node {child}}
    };
\end{tikzpicture}
\end{codeexample}
  \itemstyle{edge from parent fork left}
  behaves similary. 
  \itemstyle{edge from parent fork up}
  behaves similary. 
\end{itemize}



\subsection{Background Library}

\label{section-tikz-backgrounds}

\begin{package}{pgflibrarytikzbackgrounds}
  This packages defines ``backgrounds'' for pictures. This does not
  refer to background pictures, but rather to frames drawn around and
  behind pictures. For example, this package allows you to just add
  the |framed| option to a picture to get a rectangular box around
  your picture or |gridded| to put a grid behind your picture.
\end{package}

When this package is loaded, the following styles become available:
\begin{itemize}
  \itemstyle{show background rectangle}
  This style causes a rectangle to be drawn behind your graphic. This
  style option must be given to the |{tikzpicture}| environment or to
  the |\tikz| command.
\begin{codeexample}[]
\begin{tikzpicture}[show background rectangle]
  \draw (0,0) ellipse (10mm and 5mm);
\end{tikzpicture}
\end{codeexample}
  The size of the background rectangle is determined as follows:
  We start with the bounding box of the picture. Then, a certain
  separator distance is added on the sides. This distance can be
  different for the $x$- and $y$-directions and can be set using the
  following options:
  \begin{itemize}
    \itemoption{inner frame xsep}|=|\meta{dimension}
    Sets the additional horizontal separator distance for the
    background rectangle. The default is |1ex|.
    \itemoption{inner frame ysep}|=|\meta{dimension}
    Same for the vertical separator distance.
    \itemoption{inner frame sep}|=|\meta{dimension}
    sets the horizontal and vertical separator distances
    simultaneously. 
  \end{itemize}
  The following two styles make setting the inner separator a bit
  easier to remember:
  \begin{itemize}
    \itemstyle{tight background} Sets the inner frame separator to
    0pt. The background rectangle will have the size of the bounding
    box. 
    \itemstyle{loose background} Sets the inner frame separator to 2ex.
  \end{itemize}
    
  You can influence how the background rectangle is rendered by setting
  the following style:
  \begin{itemize}
    \itemstyle{background rectangle}
    This style dictates how the background rectangle is drawn or
    filled. By default this style is set to |draw|, which causes the
    path of the background rectangle to be drawn in the usual
    way. Setting this style to, say, |fill=blue!20| causes a light
    blue background to be added to the picture. You can also use more
    fancy settings as shown in the following example:
\begin{codeexample}[]
\tikzstyle{background rectangle}=
  [double,ultra thick,draw=red,top color=blue,rounded corners]      
\begin{tikzpicture}[show background rectangle]
  \draw (0,0) ellipse (10mm and 5mm);
\end{tikzpicture}
\end{codeexample}
    Naturally, no one in their right mind would use the above, but
    here is a nice background: 
\begin{codeexample}[]
\tikzstyle{background rectangle}=
  [draw=blue!50,fill=blue!20,rounded corners=1ex]      
\begin{tikzpicture}[show background rectangle]
  \draw (0,0) ellipse (10mm and 5mm);
\end{tikzpicture}
\end{codeexample}
\end{itemize}
  \itemstyle{framed}
  This is a shorthand for |show background rectangle|.
  \itemstyle{show background grid}
  This style behaves similarly to the |show background rectangle|
  style, but it will not use a rectangle path, but a grid. The lower
  left and upper right corner of the grid is computed in the same way
  as for the background rectangle:
\begin{codeexample}[]
\begin{tikzpicture}[show background grid]
  \draw (0,0) ellipse (10mm and 5mm);
\end{tikzpicture}
\end{codeexample}
  You can influence the background grid by setting
  the following style:
  \begin{itemize}
    \itemstyle{background grid}
    This style dictates how the background grid path is drawn. The
    default is |draw,help lines|. 
\begin{codeexample}[]
\tikzstyle{background grid}=[thick,draw=red,step=.5cm]
\begin{tikzpicture}[show background grid]
  \draw (0,0) ellipse (10mm and 5mm);
\end{tikzpicture}
\end{codeexample}
  This option can be combined with the |framed| option (use the
  |framed| option first):
\begin{codeexample}[]
\tikzstyle{background grid}=[thick,draw=red,step=.5cm]
\tikzstyle{background rectangle}=[rounded corners,fill=yellow]
\begin{tikzpicture}[framed,gridded]
  \draw (0,0) ellipse (10mm and 5mm);
\end{tikzpicture}
\end{codeexample}
  \itemstyle{gridded}
  This is a shorthand for |show background grid|.
  \end{itemize}
  \itemstyle{show background top}
  This style causes a single line to be drawn at the top of the
  background rectangle. Normally, the line coincides exactly with the
  top line of the background rectangle:
\begin{codeexample}[]
\tikzstyle{background rectangle}=[fill=yellow]    
\begin{tikzpicture}[framed,show background top]
  \draw (0,0) ellipse (10mm and 5mm);
\end{tikzpicture}
\end{codeexample}
  The following option allows you to lengthen (or shorten) the line:
  \begin{itemize}
    \itemoption{outer frame xsep}|=|\meta{dimension}
    The \meta{dimension} is added at the left and right side of the
    line. 
\begin{codeexample}[]
\tikzstyle{background rectangle}=[fill=yellow]    
\begin{tikzpicture}
    [framed,show background top,outer frame xsep=1ex]
  \draw (0,0) ellipse (10mm and 5mm);
\end{tikzpicture}
\end{codeexample}
    \itemoption{outer frame ysep}|=|\meta{dimension}
    This option does not apply to the top line, but to the left and
    right lines, see below.
    \itemoption{outer frame sep}|=|\meta{dimension}
    Sets both the $x$- and $y$-separation.
\begin{codeexample}[]
\tikzstyle{background rectangle}=[fill=blue!20]    
\begin{tikzpicture}
  [outer frame sep=1ex,%
   show background top,%
   show background bottom,%
   show background left,%
   show background right]
  \draw (0,0) ellipse (10mm and 5mm);
\end{tikzpicture}
\end{codeexample}
  \end{itemize}
  You can influence how the line is drawn grid by setting
  the following style:
  \begin{itemize}
    \itemstyle{background top}
    Default is |draw|.
\begin{codeexample}[]
\tikzstyle{background rectangle}=[fill=blue!20]    
\tikzstyle{background top}=[draw=blue!50,line width=1ex]
\begin{tikzpicture}[framed,show background top]
  \draw (0,0) ellipse (10mm and 5mm);
\end{tikzpicture}
\end{codeexample}
  \end{itemize}
  \itemstyle{show background bottom}
  works like the style for the top line.
  \itemstyle{show background left}
  works like the style for the top line.
  \itemstyle{show background right}
  works like the style for the top line.
\end{itemize}


\subsection{Automata Drawing Library}

\begin{package}{pgflibraryautomata}
  This packages, which is still under construction, provides shapes
  and styles for drawing automata.
\end{package}


\begin{shape}{state}
  This shape is currently just an alias for the |circle|
  shape. However, it is useful to have another shape |state| since one
  can then say, for example, the the style |every state shape| should
  be set to some special value.
\end{shape}

\begin{shape}{state with output}
  This shape is a multi-part shape. The main part (the text part) is
  the name of the state as in the |state| shape. The second part is
  called |output|. This node part is drawn in the lower part of the
  node, separated from the upper part by a line.
  
\begin{codeexample}[]
\begin{tikzpicture}
  \node [state with output,draw,double,fill=red!20]
  {
    $q_1$
    \nodepart{output}
    $00$
  };
\end{tikzpicture}
\end{codeexample}

  The shape inherits all anchors from the |circle| shape and defines
  the |output| anchor in addition. See also the
  following figure:
\begin{codeexample}[]
\Huge
\begin{tikzpicture}
  \node[name=s,shape=state with output,style=shape example] {state name\nodepart{output}output};
  \foreach \anchor/\placement in
    {north west/above left, north/above, north east/above right, 
     west/left, center/below, east/right, 
     mid west/right, mid/above, mid east/left, 
     base west/left, base/below, base east/right, 
     south west/below left, south/below, south east/below right, 
     text/left, output/left, 130/above}
     \draw[shift=(s.\anchor)] plot[mark=x] coordinates{(0,0)}
       node[\placement] {\scriptsize\texttt{(s.\anchor)}};
\end{tikzpicture}
\end{codeexample}
\end{shape}



%%% Local Variables: 
%%% mode: latex
%%% TeX-master: "pgfmanual-pdftex-version"
%%% End: 
