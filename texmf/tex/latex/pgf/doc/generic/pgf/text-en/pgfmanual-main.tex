% Copyright 2003 by Till Tantau <tantau@cs.tu-berlin.de>.
%
% This program can be redistributed and/or modified under the terms
% of the LaTeX Project Public License Distributed from CTAN
% archives in directory macros/latex/base/lppl.txt.

% pgf version is defined in \pgfversion in file
% generic/pgf/utilities/pgfrcs.code.tex 

\def\xcolorversion{2.00}
\def\xkeyvalversion{1.8}

\usepackage[version=0.96]{pgf}
\usepackage{tikz}
\usepackage{pgflibraryarrows}
\usepackage{pgflibraryshapes}
\usepackage{pgflibraryplotmarks}
\usepackage{pgflibrarytikzbackgrounds}
\usepackage{pgflibrarytikztrees}
\usepackage[left=2.25cm,right=2.25cm,top=2.5cm,bottom=2.5cm,nohead]{geometry}
\usepackage{amsmath,amssymb}
\usepackage{xxcolor}
\usepackage{pifont}
\usepackage{makeidx}
\usepackage[latin1]{inputenc}
\usepackage{amsmath}

\input{../../macros/pgfmanual-macros}

\makeindex

\makeatletter
\renewcommand*\l@subsection{\@dottedtocline{2}{1.5em}{2.8em}}
\renewcommand*\l@subsubsection{\@dottedtocline{3}{4.3em}{3.2em}}
\makeatother

%\includeonly{pgfmanual-libraries}

% Global styles:
\tikzstyle{every plot}=[prefix=plots/pgf-]
\tikzstyle{shape example}=[color=black!30,draw,fill=yellow!30,line width=.5cm,inner xsep=2.5cm,inner ysep=0.5cm]

\index{Options for graphics|see{Graphic options}}
\index{Options for packages|see{Package options}}
\index{File|see{Packages and files}}
\index{Layout|see{Page layout}}

\begin{document}

{
  \parindent0pt
\vbox{}
\vskip 3.5cm
\Huge
\tikzname\ and \pgfname

\Large
Manual for Version \pgfversion

\vskip 3cm 

\begin{codeexample}[graphic=white]
\tikz[rotate=30]
  \foreach \x / \xcolor in {0/blue,1/cyan,2/green,3/yellow,4/red}
    \foreach \y / \ycolor in {0/blue,1/cyan,2/green,3/yellow,4/red}
      \shade[ball color=\xcolor!50!\ycolor] (\x,\y) circle (7.5mm);
\end{codeexample}
\vskip 0cm plus 1.5fill
\vbox{}         
\clearpage
}

{
  \vbox{}
  \vskip0pt plus 1fill
  F�r meinen Vater, damit er noch viele sch�ne \TeX-Graphiken erschaffen kann.
  \vskip0pt plus 3fill
  \vbox{}
  \clearpage
}


\title{The \tikzname\ and \pgfname\ Packages\\
  Manual for Version \pgfversion\\[1mm]
\large\href{http://sourceforge.net/projects/pgf}{\texttt{http://sourceforge.net/projects/pgf}}}
\author{Till Tantau\\
  \href{mailto:tantau@users.sourceforge.net}{\texttt{tantau@users.sourceforge.net}}}

\maketitle

\tableofcontents

\clearpage

\part{Getting Started}

This part is intended to help you get started with the \pgfname\
package. First, the installation process is explained; however, the
system will typically be already installed on your system, so this can
often be skipped. Next, a short tutorial is given that explains the
most often used commands and concepts of \tikzname, without going into
any of the glorious details. At the end of this section you will find
some, hopefully useful, hints on how to create ``good'' graphics in
general. The information in this section is not specific to
\pgfname. 

\vskip3cm

\begin{codeexample}[graphic=white,width=0pt]
\tikz \draw[thick,rounded corners=8pt]
  (0,0) -- (0,2) -- (1,3.25) -- (2,2) -- (2,0) -- (0,2) -- (2,2) -- (0,0) -- (2,0);
\end{codeexample}

\include{pgfmanual-introduction}
\include{pgfmanual-installation}
\include{pgfmanual-tutorial}
\include{pgfmanual-guidelines}
\include{pgfmanual-drivers}


\part{Ti\emph{k}Z ist \emph{kein} Zeichenprogramm}
\label{part-tikz}

\vskip3cm
\begin{codeexample}[graphic=white]
\begin{tikzpicture}
  \draw[fill=yellow] (0,0) -- (60:.75cm) arc (60:180:.75cm);
  \draw(120:0.4cm) node {$\alpha$};

  \draw[fill=green!30] (0,0) -- (right:.75cm) arc (0:60:.75cm);
  \draw(30:0.5cm) node {$\beta$};

  \begin{scope}[shift={(60:2cm)}]
    \draw[fill=green!30] (0,0) -- (180:.75cm) arc (180:240:.75cm);
    \draw (30:-0.5cm) node {$\gamma$};

    \draw[fill=yellow] (0,0) -- (240:.75cm) arc (240:360:.75cm);
    \draw (-60:0.4cm) node {$\delta$};
  \end{scope}

  \begin{scope}[thick]
    \draw  (60:-1cm) node[fill=white] {$E$} -- (60:3cm) node[fill=white] {$F$};
    \draw[red]                   (-2,0) node[left] {$A$} -- (3,0) node[right]{$B$};
    \draw[blue,shift={(60:2cm)}] (-3,0) node[left] {$C$} -- (2,0) node[right]{$D$};
  
    \draw[shift={(60:1cm)},xshift=4cm]
    node [right,text width=6cm,rounded corners,fill=red!20,inner sep=1ex]
    {
      When we assume that $\color{red}AB$ and $\color{blue}CD$ are
      parallel, i.\,e., ${\color{red}AB} \mathbin{\|} \color{blue}CD$,
      then $\alpha = \delta$ and $\beta = \gamma$.
    };
  \end{scope}
\end{tikzpicture}
\end{codeexample}



\include{pgfmanual-tikz-design}
\include{pgfmanual-tikz-scopes}
\include{pgfmanual-tikz-coordinates}
\include{pgfmanual-tikz-paths}
\include{pgfmanual-tikz-actions}
\include{pgfmanual-tikz-shapes}
\include{pgfmanual-tikz-trees}
\include{pgfmanual-tikz-transformations}



\part{Libraries and Utilities}
\label{part-libraries}

In this part the library and utility packages are documented. The
library packages provide additional predefined graphic objects like
new arrow heads, or new plot marks. These are not loaded by default
since many users will not need them.

The utility packages are not directly involved in creating graphics,
but you may find them useful nonetheless. All of them either directly
depend on \pgfname\ or they are designed to work well together with
\pgfname\ even though they can be used in a stand-alone way.
\vskip2cm
\medskip
\noindent
\begin{codeexample}[graphic=white]
\begin{tikzpicture}[scale=2]
  \shade[top color=blue,bottom color=gray!50] (0,0) parabola (1.5,2.25) |- (0,0);
  \draw (1.05cm,2pt) node[above] {$\displaystyle\int_0^{3/2} \!\!x^2\mathrm{d}x$};
  
  \draw[style=help lines] (0,0) grid (3.9,3.9)
       [step=0.25cm]      (1,2) grid +(1,1);

  \draw[->] (-0.2,0) -- (4,0) node[right] {$x$};
  \draw[->] (0,-0.2) -- (0,4) node[above] {$f(x)$};

  \foreach \x/\xtext in {1/1, 1.5/1\frac{1}{2}, 2/2, 3/3}
    \draw[shift={(\x,0)}] (0pt,2pt) -- (0pt,-2pt) node[below] {$\xtext$};

  \foreach \y/\ytext in {1/1, 2/2, 2.25/2\frac{1}{4}, 3/3}
    \draw[shift={(0,\y)}] (2pt,0pt) -- (-2pt,0pt) node[left] {$\ytext$};
    
  \draw (-.5,.25) parabola bend (0,0) (2,4) node[below right] {$x^2$};
\end{tikzpicture}
\end{codeexample}

\include{pgfmanual-libraries}
\include{pgfmanual-pgffor}
\include{pgfmanual-pages}
\include{pgfmanual-xxcolor}



\part{The Basic Layer}

\vskip1cm
\begin{codeexample}[graphic=white]
\begin{tikzpicture}
  \draw[gray,very thin] (-1.9,-1.9) grid (2.9,3.9)
          [step=0.25cm] (-1,-1) grid (1,1);
  \draw[blue] (1,-2.1) -- (1,4.1); % asymptote
                
  \draw[->] (-2,0) -- (3,0) node[right] {$x(t)$};
  \draw[->] (0,-2) -- (0,4) node[above] {$y(t)$};

  \foreach \pos in {-1,2}
    \draw[shift={(\pos,0)}] (0pt,2pt) -- (0pt,-2pt) node[below] {$\pos$};

  \foreach \pos in {-1,1,2,3}
    \draw[shift={(0,\pos)}] (2pt,0pt) -- (-2pt,0pt) node[left] {$\pos$};

  \fill (0,0) circle (0.064cm);
  \draw[thick,parametric,domain=0.4:1.5,samples=200]
    % The plot is reparameterised such that there are more samples
    % near the center.
    plot[id=asymptotic-example] function{(t*t*t)*sin(1/(t*t*t)),(t*t*t)*cos(1/(t*t*t))}
    node[right] {$\bigl(x(t),y(t)\bigr) = (t\sin \frac{1}{t}, t\cos \frac{1}{t})$};

  \fill[red] (0.63662,0) circle (2pt)
    node [below right,fill=white,yshift=-4pt] {$(\frac{2}{\pi},0)$};
\end{tikzpicture}
\end{codeexample}


\include{pgfmanual-base-design}
\include{pgfmanual-base-scopes}
\include{pgfmanual-base-points}
\include{pgfmanual-base-paths}
\include{pgfmanual-base-actions}
\include{pgfmanual-base-arrows}
\include{pgfmanual-base-nodes}
\include{pgfmanual-base-transformations}
\include{pgfmanual-base-images}
\include{pgfmanual-base-shadings}
\include{pgfmanual-base-plots}
\include{pgfmanual-base-layers}
\include{pgfmanual-base-quick}




\part{The System Layer}
\label{part-system}

This part describes the low-level interface of \pgfname, called the
\emph{system layer}. This interface provides a complete abstraction of
the internals of the underlying drivers. 

Unless you intend to port \pgfname\ to another driver or unless you intend
to write your own optimized frontend, you need not read this part.

In the following it is assumed that you are familiar with the basic
workings of the |graphics| package and that you know what
\TeX-drivers are and how they work.

\vskip1cm
\begin{codeexample}[graphic=white]
\begin{tikzpicture}[shorten >=1pt,->]
  \tikzstyle{vertex}=[circle,fill=black!25,minimum size=17pt,inner sep=0pt]
  
  \foreach \name/\x in {s/1, 2/2, 3/3, 4/4, 15/11, 16/12, 17/13, 18/14, 19/15, t/16}
    \node[vertex] (G-\name) at (\x,0) {$\name$};

  \foreach \name/\angle/\text in {P-1/234/5, P-2/162/6, P-3/90/7, P-4/18/8, P-5/-54/9}
    \node[vertex,xshift=6cm,yshift=.5cm] (\name) at (\angle:1cm) {$\text$};
  
  \foreach \name/\angle/\text in {Q-1/234/10, Q-2/162/11, Q-3/90/12, Q-4/18/13, Q-5/-54/14}
    \node[vertex,xshift=9cm,yshift=.5cm] (\name) at (\angle:1cm) {$\text$};

  \foreach \from/\to in {s/2,2/3,3/4,3/4,15/16,16/17,17/18,18/19,19/t}
    \draw (G-\from) -- (G-\to);  

  \foreach \from/\to in {1/2,2/3,3/4,4/5,5/1,1/3,2/4,3/5,4/1,5/2}
    { \draw (P-\from) -- (P-\to); \draw (Q-\from) -- (Q-\to); }

  \draw (G-3) .. controls +(-30:2cm) and +(-150:1cm) .. (Q-1);
  \draw (Q-5) -- (G-15);
\end{tikzpicture}
\end{codeexample}

\include{pgfmanual-pgfsys-overview}
\include{pgfmanual-pgfsys-commands}
\include{pgfmanual-pgfsys-paths}
\include{pgfmanual-pgfsys-protocol}



\part{References and Index}

\vskip1cm
\begin{codeexample}[graphic=white]
\begin{tikzpicture}
  \draw[line width=0.3cm,color=red!30,cap=round,join=round] (0,0)--(2,0)--(2,5);
  \draw[help lines] (-2.5,-2.5) grid (5.5,7.5);
  \draw[very thick] (1,-1)--(-1,-1)--(-1,1)--(0,1)--(0,0)--
    (1,0)--(1,-1)--(3,-1)--(3,2)--(2,2)--(2,3)--(3,3)--
    (3,5)--(1,5)--(1,4)--(0,4)--(0,6)--(1,6)--(1,5)
    (3,3)--(4,3)--(4,5)--(3,5)--(3,6)
    (3,-1)--(4,-1);
  \draw[below left] (0,0) node(s){$s$};
  \draw[below left] (2,5) node(t){$t$};
  \fill (0,0) circle (0.06cm) (2,5) circle (0.06cm);
  \draw[->,rounded corners=0.2cm,shorten >=2pt]
    (1.5,0.5)-- ++(0,-1)-- ++(1,0)-- ++(0,2)-- ++(-1,0)-- ++(0,2)-- ++(1,0)--
    ++(0,1)-- ++(-1,0)-- ++(0,-1)-- ++(-2,0)-- ++(0,3)-- ++(2,0)-- ++(0,-1)--
    ++(1,0)-- ++(0,1)-- ++(1,0)-- ++(0,-1)-- ++(1,0)-- ++(0,-3)-- ++(-2,0)--
    ++(1,0)-- ++(0,-3)-- ++(1,0)-- ++(0,-1)-- ++(-6,0)-- ++(0,3)-- ++(2,0)--
    ++(0,-1)-- ++(1,0);
\end{tikzpicture}
\end{codeexample}

\printindex

\end{document}


